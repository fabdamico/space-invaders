package com.fabiodamico.spaceinvders.test.service.impl;

import com.fabiodamico.spaceinvders.model.Matrix;
import com.fabiodamico.spaceinvders.service.impl.MapperServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;


/**
 * Unit tests for {@link MapperServiceImpl} to verify the proper mapping
 * and construction of the matrix from a text file.
 *
 * @author fabdamico.dev
 */
public class MapperServiceTest {

    private MapperServiceImpl mapperService;

    private Path alienPath;
    private Path shipPath;
    private Path radarPath;
    private Path emptyAlien;

    @Before
    public void setUp() throws Exception {
        alienPath = Paths.get(getClass().getClassLoader().getResource("alien").toURI());
        shipPath = Paths.get(getClass().getClassLoader().getResource("ship").toURI());
        radarPath = Paths.get(getClass().getClassLoader().getResource("radar").toURI());
        emptyAlien = Paths.get(getClass().getClassLoader().getResource("emptyAlien").toURI());
        mapperService = new MapperServiceImpl();
    }

    @Test
    public void mapToMatrix_should_return_matrix_obj_representing_alien() throws Exception {

        List<String> allLines = Files.readAllLines(alienPath);
        Matrix matrix = mapperService.mapToMatrix(allLines);

        assertThat((matrix.getNumCols()), is(11));
        assertThat((matrix.getNumRows()), is(8));

    }

    @Test
    public void mapToMatrix_should_return_matrix_obj_representing_ship() throws Exception {

        List<String> allLines = Files.readAllLines(shipPath);
        Matrix matrix = mapperService.mapToMatrix(allLines);

        assertThat(matrix.getNumCols(), is(8));
        assertThat(matrix.getNumRows(), is(8));

    }

    @Test
    public void mapToMatrix_should_return_matrix_obj_representing_radar() throws Exception {

        List<String> allLines = Files.readAllLines(radarPath);
        Matrix matrix = mapperService.mapToMatrix(allLines);

        assertThat(matrix.getNumCols(), is(100));
        assertThat(matrix.getNumRows(), is(50));

    }

    @Test(expected = IllegalArgumentException.class)
    public void mapToMatrix_should_throw_IllegalArgumentException_when_input_is_empty() throws Exception {
        List<String> allLines = Files.readAllLines(emptyAlien);
        mapperService.mapToMatrix(allLines);
    }

    @Test(expected = IllegalArgumentException.class)
    public void mapToMatrix_should_throw_IllegalArgumentException_with_null_list() throws Exception {
        mapperService.mapToMatrix(null);
    }
}
