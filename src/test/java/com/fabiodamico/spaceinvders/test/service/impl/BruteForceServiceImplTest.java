package com.fabiodamico.spaceinvders.test.service.impl;

import com.fabiodamico.spaceinvders.exception.EmptyMatrixException;
import com.fabiodamico.spaceinvders.model.InvaderRadarResult;
import com.fabiodamico.spaceinvders.model.Matrix;
import com.fabiodamico.spaceinvders.service.impl.BruteForceServiceImpl;
import com.fabiodamico.spaceinvders.service.impl.MapperServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;

/**
 * Unit test for{@link BruteForceServiceImpl}
 *
 * @author fabdamico.dev
 */
public class BruteForceServiceImplTest {

    private BruteForceServiceImpl bruteForceService;
    private Path alienPath;
    private Path shipPath;
    private Path realRadarPath;
    private Path simpleRadarWithAlienPath;
    private Path simpleRadarWithShipPath;
    private MapperServiceImpl mapperService;

    @Before
    public void setUp() throws Exception {
        bruteForceService = new BruteForceServiceImpl();
        alienPath = Paths.get(getClass().getClassLoader().getResource("alien").toURI());
        shipPath = Paths.get(getClass().getClassLoader().getResource("ship").toURI());
        realRadarPath = Paths.get(getClass().getClassLoader().getResource("radar").toURI());
        simpleRadarWithAlienPath = Paths.get(getClass().getClassLoader().getResource("simple_radar").toURI());
        simpleRadarWithShipPath = Paths.get(getClass().getClassLoader().getResource("simple_radar_2").toURI());
        bruteForceService = new BruteForceServiceImpl();
        mapperService = new MapperServiceImpl();
        bruteForceService.setThreshold(80);
    }

    @Test(expected = IllegalArgumentException.class)
    public void detectSpaceInvader_should_throw_IllegalArgumentException_with_null_args() throws Exception {
        bruteForceService.detectSpaceInvader(null, null);
    }

    @Test(expected = EmptyMatrixException.class)
    public void detectSpaceInvader_should_return_emptyList_with_empty_matrix() throws Exception {
        bruteForceService.detectSpaceInvader(new Matrix(0, 0), new Matrix(0, 0));
    }

    @Test
    public void detectSpaceInvader_should_return_one_simple_alien() throws EmptyMatrixException, IOException {

        Matrix alienMatrix = createMatrixFromFile(alienPath, mapperService);
        Matrix simpleRadarMatrix = createMatrixFromFile(simpleRadarWithAlienPath, mapperService);

        List<InvaderRadarResult> invaderRadarResults = bruteForceService.detectSpaceInvader(simpleRadarMatrix, alienMatrix);
        assertThat(invaderRadarResults.size(), is(1));
        assertThat(invaderRadarResults.get(0).getConfidence(), is(100.0));
    }


    @Test
    public void detectSpaceInvader_should_return_one_simple_ship() throws EmptyMatrixException, IOException {

        Matrix shipMatrix = createMatrixFromFile(shipPath, mapperService);
        Matrix simpleRadarMatrix = createMatrixFromFile(simpleRadarWithShipPath, mapperService);

        List<InvaderRadarResult> invaderRadarResults = bruteForceService.detectSpaceInvader(simpleRadarMatrix, shipMatrix);
        assertThat(invaderRadarResults.size(), is(1));
        assertThat(invaderRadarResults.get(0).getConfidence(), is(100.0));
    }

    @Test
    public void detectSpaceInvader_should_return_invaders_with_confidence_greater_than_or_equal_threshold() throws EmptyMatrixException, IOException {

        Matrix shipMatrix = createMatrixFromFile(shipPath, mapperService);
        Matrix alienMatrix = createMatrixFromFile(alienPath, mapperService);
        Matrix simpleRadarMatrix = createMatrixFromFile(realRadarPath, mapperService);

        List<InvaderRadarResult> invaderRadarResultsForShip = bruteForceService.detectSpaceInvader(simpleRadarMatrix, shipMatrix);
        List<InvaderRadarResult> invaderRadarResultsForAlien = bruteForceService.detectSpaceInvader(simpleRadarMatrix, alienMatrix);
        assertThat(invaderRadarResultsForShip.size(), is(4));
        assertThat(invaderRadarResultsForAlien.size(), is(3));

        for (InvaderRadarResult invaderRadarResult : invaderRadarResultsForAlien) {
            assertThat(invaderRadarResult.getConfidence(), greaterThanOrEqualTo(bruteForceService.getThreshold()));
        }

        for (InvaderRadarResult invaderRadarResult : invaderRadarResultsForShip) {
            assertThat(invaderRadarResult.getConfidence(), greaterThanOrEqualTo(bruteForceService.getThreshold()));
        }

        assertThat(bruteForceService.getThreshold(), is(80.0));
    }

    @Test
    public void detectSpaceInvader_should_return_invaders_with_greater_threshold() throws EmptyMatrixException, IOException {

        bruteForceService.setThreshold(90);

        Matrix alienMatrix = createMatrixFromFile(alienPath, mapperService);
        Matrix simpleRadarMatrix = createMatrixFromFile(realRadarPath, mapperService);


        List<InvaderRadarResult> invaderRadarResultsForAlien = bruteForceService.detectSpaceInvader(simpleRadarMatrix, alienMatrix);

        assertThat(invaderRadarResultsForAlien.size(), is(1));

        for (InvaderRadarResult invaderRadarResult : invaderRadarResultsForAlien) {
            assertThat(invaderRadarResult.getConfidence(), greaterThanOrEqualTo(bruteForceService.getThreshold()));
        }

        assertThat(bruteForceService.getThreshold(), is(90.0));
    }

    @Test
    public void detectSpaceInvader_should_return_0_invaders_with_highest_threshold() throws EmptyMatrixException, IOException {

        bruteForceService.setThreshold(100);

        Matrix shipMatrix = createMatrixFromFile(shipPath, mapperService);
        Matrix alienMatrix = createMatrixFromFile(alienPath, mapperService);
        Matrix simpleRadarMatrix = createMatrixFromFile(realRadarPath, mapperService);

        List<InvaderRadarResult> invaderRadarResultsForShip = bruteForceService.detectSpaceInvader(simpleRadarMatrix, shipMatrix);
        List<InvaderRadarResult> invaderRadarResultsForAlien = bruteForceService.detectSpaceInvader(simpleRadarMatrix, alienMatrix);
        assertThat(invaderRadarResultsForShip.size(), is(0));
        assertThat(invaderRadarResultsForAlien.size(), is(0));
        assertThat(bruteForceService.getThreshold(), is(100.0));
    }


    private static Matrix createMatrixFromFile(Path resourcePath, MapperServiceImpl mapperService) throws IOException {
        List<String> allResourceLines = Files.readAllLines(resourcePath);
        return mapperService.mapToMatrix(allResourceLines);
    }

}