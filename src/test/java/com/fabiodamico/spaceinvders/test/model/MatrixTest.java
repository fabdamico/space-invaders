package com.fabiodamico.spaceinvders.test.model;

import com.fabiodamico.spaceinvders.exception.EmptyMatrixException;
import com.fabiodamico.spaceinvders.model.Matrix;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for {@link Matrix :getNumCols} and {@link Matrix:getNumRows}
 *
 * @author fabdamico.dev
 */
public class MatrixTest {

    @Test(expected = EmptyMatrixException.class)
    public void getNumRows_should_throw_EmptyMatrix_when_matrix_has_0_rows() throws Exception {
        Matrix matrixWithZeroRow = new Matrix(0, 10);
        matrixWithZeroRow.getNumRows();
    }

    @Test
    public void getNumRows_should_return_exact_number_of_rows() throws Exception {
        Matrix matrix = new Matrix(15, 10);
        assertThat(matrix.getNumRows(), is(15));
    }


    @Test(expected = EmptyMatrixException.class)
    public void getNumCols_should_throw_EmptyMatrix_when_matrix_has_0_cols() throws Exception {
        Matrix matrixWithZeroCols = new Matrix(10, 0);
        matrixWithZeroCols.getNumCols();
    }

    @Test
    public void getNumRows_should_return_exact_number_of_cols() throws Exception {
        Matrix matrix = new Matrix(15, 10);
        assertThat(matrix.getNumRows(), is(15));
        assertThat(matrix.getNumCols(), is(10));
    }
}