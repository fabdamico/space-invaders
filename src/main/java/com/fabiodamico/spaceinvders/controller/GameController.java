package com.fabiodamico.spaceinvders.controller;

import com.fabiodamico.spaceinvders.exception.EmptyMatrixException;
import com.fabiodamico.spaceinvders.model.InvaderRadarResult;
import com.fabiodamico.spaceinvders.model.Matrix;
import com.fabiodamico.spaceinvders.model.SpaceInvaderResource;
import com.fabiodamico.spaceinvders.service.MapperService;
import com.fabiodamico.spaceinvders.service.RadarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * REST controller that expose endpoints to start the game, get the actual radar and get all the known invaders.
 *
 * @author fabdamico.dev
 */
@RestController
@RequestMapping("/api")
public class GameController {

    private final Logger logger = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private SpaceInvaderResource radarResource;

    @Autowired
    private SpaceInvaderResource shipResource;

    @Autowired
    private SpaceInvaderResource alienResource;

    @Autowired
    private MapperService mapperService;

    @Autowired
    private RadarService radarService;

    /**
     * REST endpoint to start the game. It loads all resources, check for invaders and return the list of positions
     * on the radar where the invaders occured.
     */
    @RequestMapping(value = "start", method = RequestMethod.PUT)
    public ResponseEntity<List<String>> startGame() {
        List<String> result = new ArrayList<>();

        Matrix radarMatrix = mapperService.mapToMatrix(radarResource.getContent());
        Matrix shipMatrix = mapperService.mapToMatrix(shipResource.getContent());
        Matrix alienMatrix = mapperService.mapToMatrix(alienResource.getContent());

        try {

            List<InvaderRadarResult> alienRadarResults = radarService.detectSpaceInvader(radarMatrix, alienMatrix);
            List<InvaderRadarResult> shipRadarResults = radarService.detectSpaceInvader(radarMatrix, shipMatrix);

            for (InvaderRadarResult shipRadarResult : shipRadarResults) {
                result.add(shipRadarResult.toString());
            }

            for (InvaderRadarResult alienRadarResult : alienRadarResults) {
                result.add(alienRadarResult.toString());
            }
            return new ResponseEntity<>(result, HttpStatus.OK);

        } catch (EmptyMatrixException e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * REST endpoint to get the actual radar
     */
    @RequestMapping(value = "radar", method = RequestMethod.GET)
    public ResponseEntity<List<String>> getRadar() {
        return new ResponseEntity<>(radarResource.getContent(), HttpStatus.OK);
    }

    /**
     * REST endpoint to get all known invaders
     */
    @RequestMapping(value = "invaders", method = RequestMethod.GET)
    public ResponseEntity<List<List<String>>> getInvaders() {
        List<List<String>> result = new ArrayList<>();
        result.add(alienResource.getContent());
        result.add(shipResource.getContent());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
