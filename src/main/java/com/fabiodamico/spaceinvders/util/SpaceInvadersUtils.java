package com.fabiodamico.spaceinvders.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Utility class
 *
 * @author fabdamico.dev
 */
public class SpaceInvadersUtils {

    /**
     * Utility method to extract from an inputStream a list of string with JDK8.
     * Is able to read resources located inside a JAR packed spring boot application.
     *
     * @param input stream from file
     * @return a List of String represent all lines
     */
    public static List<String> readInputStream(InputStream input) throws IOException {
        try (BufferedReader buffer = new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8))) {
            return buffer.lines().collect(Collectors.toList());
        }
    }
}
