package com.fabiodamico.spaceinvders.exception;

import com.fabiodamico.spaceinvders.model.Matrix;

/**
 * Thrown to indicate that a Matrix object is empty
 *
 * @author fabdamico.dev
 * @see Matrix
 */
public class EmptyMatrixException extends Exception {

    public EmptyMatrixException(String message) {
        super(message);
    }
}
