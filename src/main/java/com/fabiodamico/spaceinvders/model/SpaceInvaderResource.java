package com.fabiodamico.spaceinvders.model;

import java.util.List;

/**
 * Pojo class to represent a space invader resource.
 *
 * @author fabdamico.dev
 */
public class SpaceInvaderResource {

    private List<String> content;

    public SpaceInvaderResource content(List<String> content) {
        this.content = content;
        return this;
    }

    public List<String> getContent() {
        return content;
    }
}
