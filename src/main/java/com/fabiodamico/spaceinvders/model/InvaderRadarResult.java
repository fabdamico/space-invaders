package com.fabiodamico.spaceinvders.model;

import java.io.Serializable;

/**
 * Pojo class to represent a radar result.
 *
 * @author fabdamico.dev
 */
public class InvaderRadarResult implements Serializable {

    private int row;
    private int column;
    private double confidence;

    public InvaderRadarResult row(int row) {
        this.row = row;
        return this;
    }

    public InvaderRadarResult column(int column) {
        this.column = column;
        return this;
    }

    public InvaderRadarResult confidence(double confidence) {
        this.confidence = confidence;
        return this;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public double getConfidence() {
        return confidence;
    }

    @Override
    public String toString() {
        return "InvaderRadarResult{" +
                "row=" + row +
                ", column=" + column +
                ", confidence=" + confidence +
                '}';
    }
}
