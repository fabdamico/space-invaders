package com.fabiodamico.spaceinvders.model;

import com.fabiodamico.spaceinvders.exception.EmptyMatrixException;

import java.util.Arrays;

/**
 * Pojo class to represent a matrix.
 *
 * @author fabdamico.dev
 */
public class Matrix {

    private final int[][] content;

    public Matrix(int rows, int columns) {
        this.content = new int[rows][columns];
    }

    public Matrix(int[][] data) {
        this.content = Arrays.copyOf(data, data.length);
    }

    public int[][] getContent() {
        return Arrays.copyOf(content, content.length);
    }

    public int getNumRows() throws EmptyMatrixException {
        if (content.length == 0) {
            throw new EmptyMatrixException("The matrix has 0 rows");
        } else {
            return content.length;
        }
    }

    public int getNumCols() throws EmptyMatrixException {
        try {
            if (content[0].length == 0) {
                throw new EmptyMatrixException("The matrix has 0 columns");
            } else {
                return content[0].length;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            throw new EmptyMatrixException("The matrix has 0 columns");
        }
    }
}
