package com.fabiodamico.spaceinvders;

import com.fabiodamico.spaceinvders.model.SpaceInvaderResource;
import com.fabiodamico.spaceinvders.util.SpaceInvadersUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Utility bean for loading all {@link SpaceInvaderResource} in Spring context
 *
 * @author fabdamico.dev
 */
@Configuration
public class GameConfiguration {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${invadersTemplateURI.alien}")
    private String alienTemplateURI;

    @Value("${invadersTemplateURI.ship}")
    private String shipTemplateURI;

    @Value("${config.radarFilePath}")
    private String radarTemplateURI;

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public SpaceInvaderResource radarResource() {
        Path radarPath = Paths.get(radarTemplateURI);
        SpaceInvaderResource radarResource = new SpaceInvaderResource();

        try {
            radarResource.content(Files.readAllLines(radarPath));
        } catch (IOException e) {
            logger.error("Unable to read radar file", e);
        }
        return radarResource;
    }

    @Bean
    public SpaceInvaderResource alienResource() {
        InputStream inputAlienStream;
        SpaceInvaderResource alienResource = new SpaceInvaderResource();

        try {

            Resource alienResourceFromPath = resourceLoader.getResource(alienTemplateURI);
            inputAlienStream = alienResourceFromPath.getInputStream();

            alienResource.content(SpaceInvadersUtils.readInputStream(inputAlienStream));

        } catch (IOException e) {
            logger.error("Unable to find ship conf file", e);
        }
        return alienResource;
    }

    @Bean
    public SpaceInvaderResource shipResource() {
        InputStream inputShipStream;
        SpaceInvaderResource shipResource = new SpaceInvaderResource();

        try {

            Resource shipResourceFromPath = resourceLoader.getResource(shipTemplateURI);
            inputShipStream = shipResourceFromPath.getInputStream();

            shipResource.content(SpaceInvadersUtils.readInputStream(inputShipStream));

        } catch (IOException e) {
            logger.error("Unable to find ship conf file", e);
        }
        return shipResource;
    }
}
