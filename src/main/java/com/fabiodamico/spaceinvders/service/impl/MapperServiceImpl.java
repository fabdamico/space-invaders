package com.fabiodamico.spaceinvders.service.impl;

import com.fabiodamico.spaceinvders.model.Matrix;
import com.fabiodamico.spaceinvders.service.MapperService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Service implementation of {@link MapperService}.
 *
 * @author fabdamico.dev
 */
@Service
public class MapperServiceImpl implements MapperService {
    /**
     * Method to transform image based on character into binary matrix
     *
     * @param allLines list of strings from a text file
     * @return a Matrix object representing
     * @throws IllegalArgumentException in case of null or empty list passed to the method
     */
    @Override
    public Matrix mapToMatrix(List<String> allLines) {

        if (CollectionUtils.isEmpty(allLines)) {
            throw new IllegalArgumentException("Empty or null list passed to mapToMatrix");
        }

        int row = allLines.size();
        String max = Collections.max(allLines, Comparator.comparing(s -> s.length()));
        int column = max.length();

        int matrix[][] = new int[row][column];

        for (int x = 0; x < row; x++) {
            char[] chars = allLines.get(x).toCharArray();
            for (int y = 0; y < chars.length; y++) {
                matrix[x][y] = convertToBinary((chars[y]));
            }
        }

        return new Matrix(matrix);
    }

    private int convertToBinary(char aChar) {
        if (aChar == '-') {
            return 1;
        } else {
            return 0;
        }
    }
}
