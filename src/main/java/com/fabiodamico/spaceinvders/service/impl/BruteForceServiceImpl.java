package com.fabiodamico.spaceinvders.service.impl;

import com.fabiodamico.spaceinvders.exception.EmptyMatrixException;
import com.fabiodamico.spaceinvders.model.InvaderRadarResult;
import com.fabiodamico.spaceinvders.model.Matrix;
import com.fabiodamico.spaceinvders.service.RadarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementation of {@link RadarService} which provide a brute-force algorithm
 * to find a subMatrix inside a matrix. The service is using a pre-setted threshold because there is some noise
 * in the radar.
 *
 * @author fabdamico.dev
 */
@Service
public class BruteForceServiceImpl implements RadarService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${config.threshold}")
    private double threshold;

    /**
     * Brute-force algorithm to find subMatrix into a given matrix
     *
     * @param matrix    representing the radar
     * @param subMatrix representing the invader
     * @return list of {@link InvaderRadarResult}
     * @throws IllegalArgumentException in case of null matrix passed to method
     */
    @Override
    public List<InvaderRadarResult> detectSpaceInvader(Matrix matrix, Matrix subMatrix) throws EmptyMatrixException {
        if (matrix == null || subMatrix == null) {
            throw new IllegalArgumentException("Null matrix passed as argument");
        }

        int[][] radarMatrix = matrix.getContent();
        int[][] invaderMatrix = subMatrix.getContent();
        List<InvaderRadarResult> invaderRadarResults = new ArrayList<>();

        if (radarMatrix.length == 0 || invaderMatrix.length == 0) {
            throw new EmptyMatrixException("Empty matrix passed to detectSpaceInvader method");
        }

        for (int anchorRow = 0; anchorRow < radarMatrix.length - invaderMatrix.length + 1; anchorRow++) {

            for (int anchorColumn = 0; anchorColumn < radarMatrix[0].length - invaderMatrix[0].length + 1; anchorColumn++) {

                int matchingPixels = 0;

                for (int invaderRow = 0; invaderRow < invaderMatrix.length; invaderRow++) {

                    for (int invaderColumn = 0; invaderColumn < invaderMatrix[0].length; ++invaderColumn) {

                        int radarRow = anchorRow + invaderRow;
                        int radarColumn = anchorColumn + invaderColumn;


                        if (radarMatrix[radarRow][radarColumn] == invaderMatrix[invaderRow][invaderColumn]) {
                            matchingPixels++;
                        }
                    }
                }
                double matchingPixelsPercentage = (matchingPixels * 100d) / (invaderMatrix.length * invaderMatrix[0].length);

                if (matchingPixelsPercentage >= threshold) {
                    InvaderRadarResult invaderRadarResult = new InvaderRadarResult()
                            .column(anchorColumn)
                            .row(anchorRow)
                            .confidence(matchingPixelsPercentage);

                    invaderRadarResults.add(invaderRadarResult);
                }
            }
        }
        return invaderRadarResults;
    }

    public void setThreshold(double threshold) {
        this.threshold = threshold;
    }

    public double getThreshold() {
        return threshold;
    }
}
