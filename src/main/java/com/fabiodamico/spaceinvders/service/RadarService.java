package com.fabiodamico.spaceinvders.service;

import com.fabiodamico.spaceinvders.exception.EmptyMatrixException;
import com.fabiodamico.spaceinvders.model.InvaderRadarResult;
import com.fabiodamico.spaceinvders.model.Matrix;

import java.util.List;

/**
 * Radar service to detect a space invader in a given matrix.
 *
 * @author fabdamico.dev
 */
public interface RadarService {
    List<InvaderRadarResult> detectSpaceInvader(Matrix matrix, Matrix subMatrix) throws EmptyMatrixException;
}
