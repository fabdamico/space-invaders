package com.fabiodamico.spaceinvders.service;

import com.fabiodamico.spaceinvders.model.Matrix;

import java.util.List;

/**
 * Service to map an image based on char into a binary matrix obj
 *
 * @author fabdamico.dev
 * @see Matrix
 */

public interface MapperService {

    Matrix mapToMatrix(List<String> list);
}
